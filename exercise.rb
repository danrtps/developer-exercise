class Exercise

  # Assume that "str" is a sequence of words separated by spaces.
  # Return a string in which every word in "str" that exceeds 4 characters is replaced with "marklar".
  # If the word being replaced has a capital first letter, it should instead be replaced with "Marklar".
  def self.marklar(str)
  
	# split the string into an array at the spaces
	sentence = str.split(' ')
	
	# for each element in the array
	sentence.collect! { |word|
		
		# get the punctuation if it exists
		punc = (/[,.:;?!]/ =~ word[-1]) ? word[-1] : ''
		
		# strip the word of punctuation so we do not count it in the word length; consider the case when: word = 'what?'
		word = (/[,.:;?!]/ =~ word[-1]) ? word.chop : word
		
		# test the element for the variety of 'marklar', append the punctuation and replace it in the array
		(word.length > 4) ? (word == word.capitalize) ? 'Marklar' << punc : 'marklar' << punc : word << punc
	}
	
	#concatenate and return
	return sentence.join(' ')
  end

  # Return the sum of all even numbers in the Fibonacci sequence, up to
  # the "nth" term in the sequence
  # eg. the Fibonacci sequence up to 6 terms is (1, 1, 2, 3, 5, 8),
  # and the sum of its even numbers is (2 + 8) = 10
  def self.even_fibonacci(nth)
 	
	# the running total
	sum = 0
	
	# base cases
	arr = [0, 1]
	
	# iterative, space-saving algorithm for Fibonacci; we only need to store the k-1st and k-2nd terms for the kth term
	for k in 2..nth do
	
		# this is a compressed version of the commented out algorithm below
		arr[1] = arr[1] + arr.shift
		
		# calculate the kth Fibonacci number
		#f = arr[0] + arr[1]
		
		# move the k-1st Fibonacci number back
		#arr[0] = arr[1]
		
		# store the kth Fibonacci number
		#arr[1] = f

		# add conditionally
		if arr[1].even? 
			sum += arr[1]
		end
	end
	return sum 
  end
  
end